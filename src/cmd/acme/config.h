/*
    acme color theme
*/

#define C_TAGBG			0x584affFF	// Background tags
#define C_TAGFG			0x95faffFF	// Foreground tags
#define C_TAGHLBG		0x711c91FF	// Background highlight tags
#define C_TAGHLFG		0x95faffFF	// Foreground highlight tags

#define C_TXTBG			0x091833FF	// Window backround
#define C_TXTFG			0x00f3ffFF	// Window text
#define C_TXTHLBG		0xea00d9FF	// Window text highlight background
#define C_TXTHLFG		0x95faffFF	// Window text highlight foreground

#define C_COLBUTTON		0x584affFF	// Window element border
#define C_SCROLLBAR		0x133e7cFF	// Scrollbar (textsrcdraw cols reversed)

#define C_BUTTON2HL		0x6AB417FF	// Button 2 Window text highlight background UNUSED
#define C_BUTTON3HL		0xE03C28FF	// Button 3 Window text highlight background UNUSED

#define C_MODIFIED		0xea00d9FF	// File modified